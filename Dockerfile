from ubuntu:14.04
maintainer Kat McIvor 

#install packages
RUN apt-get update && apt-get clean
RUN apt-get install -y openjdk-7-jdk wget git

#install maven
#RUN wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
RUN wget https://archive.apache.org/dist/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
RUN tar xzf apache-maven-3.3.3-bin.tar.gz -C /usr/local
RUN ln -s /usr/local/apache-maven-3.3.3 /usr/local/maven
RUN ln -s /usr/local/maven/bin/mvn /bin/mvn

#get the repository
run git clone https://bitbucket.org/kizzie/hello-springboot

#change directory to spring boot
workdir hello-springboot

#compile it all
run mvn compile

#set up the start point
#entrypoint ['mvn', springboot:run']
cmd mvn springboot:run

#expose the port
expose 8080
